package pt.ua.prog2;


public class Contacto {
	
	public Contacto(String name,String number){
		nome = name;
		telefone =number;
		eMail = "";
		contactsCounter++;
	}
	public Contacto(String name,String number,String email){
		
		if (name.length() == 0){
			System.out.println("Contacto inválido");
			System.exit(1);
		}
		nome = name;
		telefone = number;
		eMail = email;
		contactsCounter++;
	}
	
	public String nome(){
		return nome;
	}
	public String telefone(){
		return telefone;
	}
	public String eMail(){
		return eMail;
	}
	
	public static int contactsCounter(){
		return contactsCounter;
	}
	
	private String nome;
	private String telefone;
	private String eMail;
	private static int contactsCounter = 0;
}

