import pt.ua.prog2.*;
import java.util.*;


public class p22
{
   static Scanner read = new Scanner(System.in);
   public static void main(String[] args)
   {
      Contacto[] cl = new Contacto[4];

      String nome,numero,eMail;
      for (int i = 0; i < cl.length; i++)
      {
		  System.out.print("Nome: ");
		  nome = read.nextLine();
		  System.out.print("Telemovel: ");
		  numero = read.nextLine();
		  System.out.print("Email: ");
		  eMail = read.nextLine();
		  cl[i] = new Contacto(nome,numero,eMail);
	  }
	  
	  System.out.println("Listagem:");
	  for (int i = 0; i < cl.length; i++){
         System.out.println(cl[i].nome() + 
               ": " + cl[i].telefone() + 
               "; " + cl[i].eMail());
      }
      System.out.println("Contactos: " + Contacto.contactsCounter());
   }
}
